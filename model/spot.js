const pool = require('../config/dbConnection');
//const async = require('async');

class Spot{}

Spot.checkDBUpdate = (callback) =>{
    pool.getConnection((err,conn) => {
        if(err){
            console.log('connecting err : ',err);
            conn.release();
            return callback(err);
        }  // case : db connection error
        var sql = 'select DB_Ver as version from LOCAL_INFO order by DB_Ver desc limit 1;';
        conn.query(sql, (err,result) => {
            if(err){
                conn.release();
                return callback(err);
            }// case : query error
            var obj;
            obj = result[0];
            console.log(obj.version);
            conn.release();
            return callback(null,obj);
        });
    });
};

Spot.getSpotInfo = (callback) =>{
    pool.getConnection((err,conn) => {
        if(err){
            console.log('connectiong err : ',err);
            conn.release();
            return callback(err);
        }
        var sql = 'select '+
                'spot.SPOT_id as SPOT_id, '+
                'spot.MALE as MALE, '+
                'spot.FEMALE as FEMALE, '+
                'spot.TWYO_BELOW as TWYO_BELOW, '+
                'spot.TWNT_THRTS as TWNT_THRTS, '+
                'spot.FRTS_FFTS as FRTS_FFTS, '+
                'spot.SXTS_ABOVE as SXTS_ABOVE, '+
                'spot.SPOT_NAME as SPOT_NAME, '+
                'spot.X_POS as X_POS, '+
                'spot.Y_POS as Y_POS, '+
                'spot.GU_ID as GU_ID,'+
                'gu.GU_NAME as GU_NAME '+
                'from SPOT_INFO as spot inner join SEOUL_GU as gu '+
                'on spot.GU_ID = gu.GU_ID;';
        conn.query(sql,(err,results) => {
            if(err){
                conn.release();
                return callback(err);
            }
            var obj = {
                spotlist: []
            };
            var transformedData = {
                spotlist:[]
            };
            obj.spotlist = results;
            for(i=0; i<obj.spotlist.length/2; i++){
                var tmp = {
                    SPOT_ID : i,
                    MALE : obj.spotlist[2*i].MALE+obj.spotlist[2*i+1].MALE,
                    FEMALE : obj.spotlist[2*i].FEMALE+obj.spotlist[2*i+1].FEMALE,
                    TWYO_BELOW : obj.spotlist[2*i].TWYO_BELOW+obj.spotlist[2*i+1].TWYO_BELOW,
                    TWNT_THRTS : obj.spotlist[2*i].TWNT_THRTS+obj.spotlist[2*i+1].TWNT_THRTS,
                    FRTS_FFTS : obj.spotlist[2*i].FRTS_FFTS+obj.spotlist[2*i+1].FRTS_FFTS,
                    SXTS_ABOVE : obj.spotlist[2*i].SXTS_ABOVE+obj.spotlist[2*i+1].SXTS_ABOVE,
                    SPOT_NAME : obj.spotlist[2*i].SPOT_NAME,
                    X_POS : obj.spotlist[2*i].X_POS,
                    Y_POS : obj.spotlist[2*i].Y_POS,
                    GU_ID : obj.spotlist[2*i].GU_ID,
                    GU_NAME : obj.spotlist[2*i].GU_NAME
                };
                transformedData.spotlist.push(tmp);
            }
            //console.log(transformedData.spotlist.length);
            conn.release();
            return callback(null,transformedData.spotlist);
        });
    });
};

module.exports = Spot;
