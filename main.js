const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const spotRouter = require('./router/spot_router');

const app = express();

app.use(bodyParser.urlencoded(
    {extended : false}
));

app.use(morgan('<:method> :date User:remote-addr :url   :status'));

app.use(spotRouter);

app.use( (req,res,next) => {
    res.sendStatus(404);
});

app.use ( (err,req,res,next) =>{
    res.status(500).send({msg:err.message});
});

app.listen(3040, () =>{
    console.log("Food Truck API Server is Running PORT : 3040");
});
