const express = require('express');
const Spot = require('../model/spot.js');

var router = express.Router();

router.route('/newdata')
.get(getDB);
router.route('/newdata/checkversion')
.get(checkDBVersion);

module.exports = router;

function checkDBVersion(req,res,next){
    
    Spot.checkDBUpdate((err,result) => {
        if(err){
            console.log("error : ",err);
            return next(err);
            }
            res.send(result);
    });
}

function getDB (req, res,next){
    Spot.getSpotInfo((err,result) => {
        if(err){
            console.log("error : ",err);
            return next(err);
        }
        res.send(result);
    });
}
